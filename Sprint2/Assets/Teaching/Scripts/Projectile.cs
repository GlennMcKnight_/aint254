﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
    public float force = -8;
    public GameObject dotPrefab;
    public float gravity = 9.8f;

    private GameObject[] m_dotLine;
    private Camera m_mainCam;
    private Vector3 m_direction;
    private Rigidbody m_rigidbody;

	// Use this for initialization
	void Start () 
    {
        m_dotLine = new GameObject[10];
        m_mainCam = Camera.main;
        m_rigidbody = GetComponent<Rigidbody>();

        for (int i = 0; i < m_dotLine.Length; i++)
        {
            GameObject gameObj = Instantiate(dotPrefab) as GameObject;
            gameObj.SetActive(false);
            m_dotLine[i] = gameObj;
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if(Input.GetMouseButton(0))
        {
            Vector3 screenPos = m_mainCam.WorldToScreenPoint(transform.position);
            screenPos.z = 0;
            m_direction = (Input.mousePosition - screenPos).normalized;
            Aim(m_direction);
        }

        if(Input.GetMouseButtonUp(0))
        {
            m_rigidbody.velocity = m_direction * force;
            for (int i = 0; i < m_dotLine.Length; i++)
            {
                m_dotLine[i].SetActive(false);
            }
        }
	}

    private void Aim(Vector3 dir)
    {
        float Vx = dir.x * force;
        float Vy = dir.y * force;
        for (int i = 0; i < m_dotLine.Length; i++)
        {
            float t = i * 0.1f;
            m_dotLine[i].transform.position = new Vector3(transform.position.x + Vx * t, (transform.position.y + Vy * t) - (gravity * t * t / 2.0f), 0.0f);
            m_dotLine[i].SetActive(true);
        }
    }
}
