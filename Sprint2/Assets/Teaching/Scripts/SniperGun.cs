﻿using UnityEngine;
using System.Collections;

public class SniperGun : MonoBehaviour 
{
    public Camera camera;
    public GameObject bulletHolePrefab;

    private float m_rayDistance = 20.0f;
	
	// Update is called once per frame
	void Update () 
    {
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, m_rayDistance))
        {
            Debug.DrawRay(ray.origin, ray.direction * hit.distance, Color.red);

            if(Input.GetMouseButtonDown(0))
            {
                Instantiate(bulletHolePrefab, hit.point, Quaternion.FromToRotation(Vector3.forward, -hit.normal));
            }
        }
        else
        {
            Debug.DrawRay(ray.origin, ray.direction * m_rayDistance, Color.green);
        }
	}
}
