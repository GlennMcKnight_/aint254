﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Gun : MonoBehaviour {

    //Variables for pistol
    float pistolDamage = 1.0f;
    float pistolRange = 70.0f;
    float pistolTriggerDelay = 0.5f;
    float pistolForce = 100f;

    bool canFire = true;
    float timer = 0f;

    //serialize field to expose in editor to add a reference to player     
    [SerializeField] private GameObject entityOrCam;
    [SerializeField] public ParticleSystem muzzleFlash;
    [SerializeField] private GameObject impactObject;
    [SerializeField] private GameObject impactBlood;
    
    // Update is called once per frame
    void Update()
    {
        //If it is attached to player object then do this. Enemy fire accessed differently
        if (entityOrCam.tag == "MainCamera")
        {
            if (Input.GetButton("Fire1") && canFire == true)
            {
                Shoot();
            }
            else
            {
                fireDelay();
            }
            
        }      
       
    }

    /// <summary>
    /// Method to fire a raycast from the gun and inflict damage
    /// Activates muzzle flash particle effect and impat particle effects depending on object tag
    /// Calls FireDelay() method to paus between shots and rest canFire bool
    /// </summary>
    private void Shoot()
    {
        if (canFire)
        {
            RaycastHit shot;
           
            if (Physics.Raycast(entityOrCam.transform.position, entityOrCam.transform.forward, out shot, pistolRange))
            {     
                //Reference to enemy object
                Enemy enemy = shot.transform.GetComponent<Enemy>();

                //Apply damage if hit point is an enemy
                if (enemy)
                {
                    enemy.TakeDamage(pistolDamage);
                }

                //If theres a rigid body then apply force to object
                if (shot.rigidbody != null)
                {
                    shot.rigidbody.AddForce(-shot.normal * pistolForce);
                }

                //If a muzzle flash gameObject has been added then play it
                if (muzzleFlash)
                {
                    muzzleFlash.Play();
                }

                //Call impact method                   
                impact(shot);
                //Set can fire to false to allow for FireDelay()
                canFire = false;

            }
                        
        }       
    }

    /// <summary>
    /// Sets the kind of impact depending on object tag
    /// </summary>
    private void impact(RaycastHit shot)
    {
        if (shot.transform.gameObject.tag == "Enemy")
        {
            GameObject temp = Instantiate(impactBlood, shot.point, Quaternion.LookRotation(shot.point));
            Destroy(temp, 0.5f);
        }
        else
        {
            GameObject temp = Instantiate(impactObject, shot.point, Quaternion.LookRotation(shot.normal));
            Destroy(temp, 0.5f);            
        }
    }

    /// <summary>
    /// Method to time weapon firing time delay
    /// Public so that ENemy can use within it's gameplay loop
    /// </summary>
    private void fireDelay()
    {
        timer += Time.unscaledDeltaTime;        
        
        if (timer > pistolTriggerDelay)
        {
            canFire = true;
            timer = 0f;            
        }        
    }
    
}
