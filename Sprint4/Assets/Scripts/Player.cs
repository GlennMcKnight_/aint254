﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField] float health = 100f;

    public void TakeDamage(float amount)
    {
        if (health != 0)
        {
            health -= amount;
        }
        else
        {
            Die();
        }
    }

    private void Die()
    {
        Debug.Log("Player is dead");
    }
}
