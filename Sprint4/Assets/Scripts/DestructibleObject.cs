﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObject : MonoBehaviour {

    public float health = 5f;

    public void TakeDamage(float amount)
    {
        if (health != 0)
        {
            health -= amount;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
