﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAlert : MonoBehaviour {

    private bool hasEntity = false;
    private bool hasTargetDetected = false;

    EnemyAlert enemyAlert;
    
       

    private void OnTriggerEnter(Collider other)
    {
        //Enemy enemy = GameObject.other.CompareTag("Enemy");
        //Enemy enemy = new Enemy();
        //enemy = Enemy.FindWithTag("Enemy");
        
        //hasTargetDetected = getTargetDetected;
        
        Debug.Log("In trigger method of EnemyAlert");

        hasEntity = true;
        if (hasTargetDetected)
        {
            other.SendMessage("BeAlerted", true, SendMessageOptions.RequireReceiver);
            Debug.Log("BeAlerted has sent message supposedly");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        hasEntity = false;
    }
}
