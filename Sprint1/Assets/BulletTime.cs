﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

namespace UnityStandardAssets.Characters.ThirdPerson {
    public class BulletTime : MonoBehaviour
    {
        //Time variables for normal and slow time
        private float slowTime = 0.3f;
        private float normalTime = 1.0f;

        //Variables for length of ability and recharge
        public float abilityLength = 5.0f;
        public float rechargeTime = 5.0f;

        //Variables for timers and recharge bool
        private float abilityTimer = 0.0f;
        private float rechargeTimer = 0.0f;
        private bool isRecharge = true;
        private bool abilityUsed = false;

        [SerializeField] private ThirdPersonCharacter player;
        
        /// <summary>
        /// Check for input for slow motion key with Update function
        /// </summary>
        public void Update()
        {
            
            //If button is pressed call BulletTimrMOde method to activate ability
            if (Input.GetKeyDown(KeyCode.J)) //GetKeyDown is most responsive for gameplay
            {
                BulletTimeMode();
                Debug.Log("Run method from Update");
            }

            //Call method to reset the ability if it has been used
            if (abilityUsed)
            {
                AbilityReset();                
            }

            //Call method to time ability recharge time
            if (!isRecharge)
            {
                RechargeAbility();
            }
            
        }
        
        /// <summary>
        /// Alter the games timescale for the special ability
        /// If the button is pressed again it will reset time to normal
        /// For eiher of these it sets the state to used and the ability must be recharged before use again
        /// </summary>
        private void BulletTimeMode()
        {

            if (abilityTimer <= abilityLength)
            {
                
                if (Time.timeScale == normalTime && isRecharge == true)
                {
                    Time.timeScale = slowTime;
                    Time.fixedDeltaTime = 0.02f * Time.timeScale;                    
                }

                else if (Time.timeScale == slowTime)
                {
                    Time.timeScale = normalTime;
                    Time.fixedDeltaTime = 0.02f * Time.timeScale;                    
                }
                else
                {
                    Debug.Log("Error with BulletTime");
                }

                abilityUsed = true;
                isRecharge = false;                      

            }
            
        }

        /// <summary>
        /// Reset the timescale to normal time if ability use runs to the end of its timer
        /// Reset ability timer
        /// </summary>
        private void AbilityReset()
        {
            if (abilityTimer <= abilityLength)
            {
                abilityTimer += Time.unscaledDeltaTime;                
            }
            else
            {
                Time.timeScale = normalTime;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
                abilityTimer = 0.0f;
            }
        }

        /// <summary>
        /// Time the recharge for the ability before itcan be used again
        /// Reset the recharge timer
        /// </summary>
        private void RechargeAbility()
        {
            if(rechargeTimer <= rechargeTime)
            {
                rechargeTimer += Time.unscaledDeltaTime;
            }
            else
            {
                isRecharge = true;
                rechargeTimer = 0.0f;
            }
        }
    }
}

