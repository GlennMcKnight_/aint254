﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerDestroy : MonoBehaviour {

    //private SpecialAbility specAb;
    [SerializeField] GameObject specAb;

    private void Start()
    {        
        specAb.GetComponent<SpecialAbility>();
    }
    private void OnCollisionEnter(Collision col)
    {        
        if (col.gameObject.name == "RigidBodyFPSController")
        {
            if (gameObject.name == "SpeedPowerUp")
            {
                specAb.GetComponent<SpecialAbility>().HasPowerUpSpeed = true;
                Destroy(gameObject);                
            }
            else if (gameObject.name == "DamagePowerUp")
            {
                specAb.GetComponent<SpecialAbility>().HasPowerUpDamage = true;
                Destroy(gameObject);                
            }
        }
    }
}
