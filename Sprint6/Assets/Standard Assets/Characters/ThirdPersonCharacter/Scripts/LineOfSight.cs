﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
namespace EnemyClass {
    public class LineOfSight {

        //Line of sight raycast variables. 3 creates a cone like effect
        RaycastHit sightCentre;
        RaycastHit sightRight;
        RaycastHit sightLeft;

        //Create vector 3 to keep LOS raycasting above the ground level
        Vector3 groundClear = new Vector3(0f, 0.5f, 0f);

        //Max range for lineofsight detection
        private float m_detectionRange = 100f;

        //Get this enemy an target
        private GameObject m_thisEnemy;
        private Transform m_target;

        private Enemy enemy;
        public bool LineOfSightDetect(bool targetDetected)
        {
            //Get components from main enemy class
            m_thisEnemy = Enemy.get
    
        //Check directly in front of enemy for player
        //add ground clearance so that ray doesn't impact ground and fail to detect player
            if (Physics.Raycast(thisEnemy.transform.position + groundClear, thisEnemy.transform.forward, out sightCentre, detectionRange))
            {
                if (sightCentre.transform == target)
                {
                    //Debug.DrawLine(thisEnemy.transform.position, sightCentre.point, Color.green);
                    Debug.Log("Centre LOS hit player");
                    targetDetected = true;
                }
                else
                {
                    Debug.DrawRay(thisEnemy.transform.position + groundClear, thisEnemy.transform.forward * detectionRange, Color.red);
                }
            }

            //Check at an angle to the right of enemy for the player. Furtherest right boundary of line of sight detection
            if (Physics.Raycast(thisEnemy.transform.position + groundClear, thisEnemy.transform.forward + new Vector3(1, 0, 0), out sightRight, detectionRange))
            {
                if (sightRight.transform == target)
                {
                    Debug.Log("Right LOS hit player");
                    targetDetected = true;
                }
                else
                {
                    Debug.DrawRay(thisEnemy.transform.position + groundClear, (thisEnemy.transform.forward + new Vector3(1, 0, 0)) * detectionRange, Color.red);
                }
            }

            //Check at an angle to the left of enemy for the player. Furtherest right boundary of line of sight detection
            if (Physics.Raycast(thisEnemy.transform.position + groundClear, thisEnemy.transform.forward + new Vector3(-1, 0, 0), out sightLeft, detectionRange))
            {
                if (sightLeft.transform == target)
                {
                    Debug.Log("Left LOS hit player");
                    targetDetected = true;
                }
                else
                {
                    Debug.DrawRay(thisEnemy.transform.position + groundClear, (thisEnemy.transform.forward + new Vector3(-1, 0, 0)) * detectionRange, Color.red);
                }
            }
        }
    }
}
*/
