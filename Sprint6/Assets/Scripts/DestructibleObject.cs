﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObject : MonoBehaviour {

    //Destruction effect and health of object
    public GameObject destructionEffect;
    public float health = 5f;

    //Materials for destruction effect
    public Material wallMat;
    public Material destructionLight;
    public Material destructionHeavy;
    
    Material destructionNewMat1;
    Material destructionNewMat2;
    Material[] destructionMats = new Material[2];

    //Vector for posiiton of object
    private Vector3 position;

    public void Start()
    {
        position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 0f, this.gameObject.transform.position.z);
        destructionMats[0] = wallMat;        
    }

    /// <summary>
    /// Take Damage to wall when hit by player
    /// Add destruction effects
    /// </summary>
    /// <param name="amount"></param>
    public void TakeDamage(float amount)
    {
        if (health > 0)
        {
            health -= amount;
        }

        //Destroy game object when health reaches 0 and instantiate destructed object
        //Add destruction materials at certain intervals of health
        if (destructionEffect != null && health <= 0)
        {
            Debug.Log("destruction effect");
            
            Instantiate(destructionEffect, transform.position, transform.rotation);          

            Destroy(gameObject);
        }
        else if((health <= 3 && health > 1) && destructionLight != null)
        {
            destructionMats[1] = destructionLight;
            GetComponent<MeshRenderer>().materials = destructionMats;
        }
        else if(health == 1 && destructionHeavy != null)
        {
            destructionMats[1] = destructionHeavy;
            GetComponent<MeshRenderer>().materials = destructionMats;
        }
        
    }
}
