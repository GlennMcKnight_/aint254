﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpecialAbility : MonoBehaviour
{

    //stores boosts if player has them
    bool hasPowerupSpeed = false;
    bool hasPowerupDamage = false;

    //Screen effect object reference
    [SerializeField] GameObject screenEffect;
    //Power modifiers   
    [SerializeField] float damageModifier = 2.0f;
    [SerializeField] float triggerDelayModifier = 0.5f;
    [SerializeField] float resetTriggerDelay;

    //One ability active variable - stops multiple at once 
    bool isAbilityActive = false;

    //Instances of script objects   
    Gun gun;
    BulletTime bullTime;


    public bool HasPowerUpSpeed
    {
        get { return hasPowerupSpeed; }
        set { hasPowerupSpeed = value; }
    }

    public bool HasPowerUpDamage
    {
        get { return hasPowerupDamage; }
        set { hasPowerupSpeed = value; }
    }


    // Use this for initialization
    void Start()
    {        
        //Get referneces to scripts       
        gun = GetComponentInChildren<Gun>();
        bullTime = GetComponent<BulletTime>(); 
    }

    // Update is called once per frame
    void Update()
    {
        //If the player uses ability and ability isnt null then activate ability
        if (Input.GetKeyDown(KeyCode.Mouse2) && (hasPowerupSpeed || hasPowerupDamage) && !isAbilityActive)
        {           
            if (hasPowerupSpeed)
            {
                isAbilityActive = true;
                ActivateSpeedBoost();
            }
            else if (hasPowerupDamage)
            {
                isAbilityActive = true;
                ActivateDamageBoost();
            }
        }
                
        //Check if ability is active
        //If it is check if bullet time has finished
        //Reset all modified variables
        if (isAbilityActive)
        {
            if (GetComponent<BulletTime>().AbilityUsed)
            {
                if (hasPowerupSpeed)
                {                    
                    gun.PistolFireDelay = resetTriggerDelay;//Reset the firedelay
                    hasPowerupSpeed = false;
                    screenEffect.SetActive(false);
                }
                else if (hasPowerupDamage)
                {
                    gun.WeaponDamage = damageModifier / 2;//Reset damage dealt by player weapon
                    hasPowerupDamage = false;
                    screenEffect.SetActive(false);
                }
            }
        }        
    }
    
    /// <summary>
    /// Activate speed power up
    /// Calls bulllettime method and sets gun script variable for trigger delay
    /// </summary>
    private void ActivateSpeedBoost()
    {        
        resetTriggerDelay = gun.PistolFireDelay;//Get value to reset the triggerdelay to
        bullTime.AbilityBulletTime();//Activate bullet time  
        gun.PistolFireDelay = triggerDelayModifier;//Modify trigger delay of player weapon   
        screenEffect.SetActive(true);
    }

    /// <summary>
    /// Activates dmagae power up
    /// Calls bullettime method and sets gun script variable
    /// </summary>
    private void ActivateDamageBoost()
    {
        bullTime.AbilityBulletTime();//Activate bullet time
        gun.WeaponDamage = damageModifier;//Modify damage dealt
        screenEffect.SetActive(true);        
    }
            
}

