﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Gun : MonoBehaviour {

    //Variables for pistol
    float pistolDamage = 1.0f;//Dmage dealt when pistol hits enemy
    float pistolRange = 70.0f;//Range of Raycast shot
    float pistolTriggerDelay = 0.5f;//Delay before next shot
    float pistolForce = 100f;//Force added to rigidbody when hit
    float headshotDamage = 100f;//Damage aplied to a headshot

    //Variables for weapon
    float weaponDelay;

    bool canFire = true;
    float timer = 0f;
    float markerTimer = 0.5f;
    bool hitMark = false;

    //expose in editor to add a reference to player     
    [SerializeField] private GameObject entityOrCam;
    [SerializeField] public ParticleSystem muzzleFlash;
    [SerializeField] private GameObject impactObject;
    [SerializeField] private GameObject impactBlood;
    [SerializeField] private GameObject hitMarker;
    
    public float WeaponDamage
    {
        set { pistolDamage = value; }
    }

    public float PistolFireDelay
    {
        get { return pistolTriggerDelay; }
        set { pistolTriggerDelay = value; }
    }

    private void Start()
    {
        weaponDelay = pistolTriggerDelay;
    }
    // Update is called once per frame
    void Update()
    {
       
        //If it is the player then enter the method
        if (entityOrCam.tag == "MainCamera")
        {
            if (Input.GetButton("Fire1") && canFire == true)
            {
                Shoot();
            }
            else
            {
                FireDelay();
            }
        }

        //Calls method to reset hit marker
        if (hitMark)
        {
            HitMarkerActive();
        }
       
    }
       
    /// <summary>
    /// Method to fire a raycast from the gun and inflict damage
    /// Activates muzzle flash particle effect and impact particle effects depending on object tag
    /// </summary>
    private void Shoot()
    {
        if (canFire)
        {
            RaycastHit shot;
           
            if (Physics.Raycast(entityOrCam.transform.position, entityOrCam.transform.forward, out shot, pistolRange))
            {
                //Reference to enemy object
                Enemy enemy = shot.transform.GetComponent<Enemy>();
                Enemy head = shot.transform.GetComponentInParent<Enemy>();
                DestructibleObject obj = shot.transform.GetComponent<DestructibleObject>();
                
                //Apply damage if hit point is an enemy
                //Apply headshot damage if hit point is enemy head
                if (enemy)
                {
                    //hitMarker.SetActive(true);
                    markerTimer = 0.1f;
                    hitMark = true;

                    hitMarker.SetActive(true);
                    

                    enemy.TakeDamage(pistolDamage);
                }
                else if (shot.transform.gameObject.tag == "EnemyHead")
                {
                    //HitMarkerActive();
                    markerTimer = 0.1f;
                    hitMark = true;

                    hitMarker.SetActive(true);
                    

                    head.TakeDamage(headshotDamage);
                }
                else if(shot.transform.gameObject.tag == "Destructible" || shot.transform.gameObject.tag == "Wall")
                {
                    obj.TakeDamage(pistolDamage);
                }


                //If theres a rigid body then apply force to object
                if (shot.rigidbody != null)
                {
                    shot.rigidbody.AddForce(-shot.normal * pistolForce);
                }


                //If a muzzle flash gameObject has been added then play it
                if (muzzleFlash)
                {
                    muzzleFlash.Play();
                }

                //Call impact method                   
                Impact(shot);
                //Set can fire to false to allow for FireDelay()

                canFire = false;
            }
            
        }

    }

    /// <summary>
    /// Switches off hit marker after time has elapsed
    /// </summary>
    private void HitMarkerActive()
    {
        
        markerTimer -= Time.deltaTime;

        if(markerTimer <= 0)
        {
            hitMarker.SetActive(false);
            hitMark = false;
        }
    }

    /// <summary>
    /// Sets the kind of impact depending on object tag
    /// </summary>
    private void Impact(RaycastHit shot)
    {
        if (shot.transform.gameObject.tag == "Enemy" || shot.transform.gameObject.tag == "EnemyHead")
        {
            GameObject temp = Instantiate(impactBlood, shot.point, Quaternion.LookRotation(shot.point));
            Destroy(temp, 0.5f);
        }
        else
        {
            GameObject temp = Instantiate(impactObject, shot.point, Quaternion.LookRotation(shot.normal));
            Destroy(temp, 0.5f);            
        }
    }

    /// <summary>
    /// Method to time weapon firing time delay
    /// Public so that ENemy can use within it's gameplay loop
    /// </summary>
    private void FireDelay()
    {
        timer += Time.unscaledDeltaTime;        
        
        if (timer > pistolTriggerDelay)
        {
            canFire = true;
            timer = 0f;            
        }        
    }    
}
