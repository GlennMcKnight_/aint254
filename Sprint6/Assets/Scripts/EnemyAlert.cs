﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAlert : MonoBehaviour {

    private bool detected = false;
    
    public void Detected(bool detection)
    {
        detected = detection;

        BroadcastMessage();
    }

    public void BroadcastMessage()
    {
        if (detected)
        {
            BroadcastMessage("EnemyDetected", true);
        }
    }
}
