﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



public class Enemy : MonoBehaviour
{    

    [Range(0f, 1f)]
    public float attackProbability = 0.5f;

    [Range(0f, 1f)]
    public float hitAccuracy = 0.5f;

    public float damage = 2f;

    public float health = 10f;
    

    //
    //
    //Line of sight raycast variables. 3 creates a cone like effect
    RaycastHit sightCentre;
    RaycastHit sightRight;
    RaycastHit sightLeft;

    //Create vector 3 to keep LOS raycasting above the ground level
    Vector3 groundClear = new Vector3(0f, 0.5f, 0f);

    //Max range for lineofsight detection
    private float detectionRange = 100f;

    //Needed to allow time to attack before chasing again
    public float followDistance = 6f;

    private NavMeshAgent m_navMeshAgent;

    public Transform target;
    public GameObject thisEnemy;
    public GameObject alertEnemiesObject;

    public GameObject[] bloodSpatter;

    //Detect target bool, used later to ease required processing each Update method call
    public bool targetDetected = false;

    //
    public RigidbodyFirstPersonController player;
    [SerializeField] public GameObject weapon;
    

    private float attackDamage = 1f;
        
    //Set distance for enemy to attack player at
    public float attackDistance = 3.5f;

    private bool messageSent = false;

    bool canFire = true;
    float timer = 0f;

    

    void Start()
    {
        m_navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        target = PlayerManager.instance.player.transform;
        //GameObject enemyAlert = GameObject.Find("EnemyAlert"); 
        
        thisEnemy.transform.position += new Vector3(1, 0, 0);
        /*
        alertCollider.enabled = false;
        alertCollider.enabled = true;
        */
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
        //If no target has been detected then call LineOfSight method
        if (!targetDetected)
        {
            LineOfSight();
        }
        else
        {
            if (!messageSent)
            {
                SendMessageUpwards("Detected", true);
                messageSent = true;
                Debug.Log(messageSent);
            } 

            thisEnemy.transform.LookAt(target);            
            Chase();           
        }
                
    }
        
    /// <summary>
    /// Method to detect the player by the enemy using 3 raycasts placed to give an effective cone of detection. 
    /// </summary>       
    private void LineOfSight()
    {
        //Check directly in front of enemy for player
        //add ground clearance so that ray doesn't impact ground and fail to detect player
        if (Physics.Raycast(thisEnemy.transform.position + groundClear, thisEnemy.transform.forward, out sightCentre, detectionRange))
        {
            if (sightCentre.transform == target)
            {
                //Debug.DrawLine(thisEnemy.transform.position, sightCentre.point, Color.green);
                Debug.Log("Centre LOS hit player");                
                targetDetected = true;                
            }
            else
            {
                Debug.DrawRay(thisEnemy.transform.position + groundClear, thisEnemy.transform.forward * detectionRange, Color.red);
            }
        }

        //Check at an angle to the right of enemy for the player. Furtherest right boundary of line of sight detection
        if (Physics.Raycast(thisEnemy.transform.position + groundClear, thisEnemy.transform.forward + new Vector3(1, 0, 0), out sightRight, detectionRange))
        {
            if (sightRight.transform == target)
            {
                Debug.Log("Right LOS hit player");                
                targetDetected = true;               
            }
            else
            {
                Debug.DrawRay(thisEnemy.transform.position + groundClear, (thisEnemy.transform.forward + new Vector3(1, 0, 0)) * detectionRange, Color.red);
            }
        }

        //Check at an angle to the left of enemy for the player. Furtherest right boundary of line of sight detection
        if (Physics.Raycast(thisEnemy.transform.position + groundClear, thisEnemy.transform.forward + new Vector3(-1, 0, 0), out sightLeft, detectionRange))
        {
            if (sightLeft.transform == target)
            {
                Debug.Log("Left LOS hit player");                
                targetDetected = true;                
            }
            else
            {
                Debug.DrawRay(thisEnemy.transform.position + groundClear, (thisEnemy.transform.forward + new Vector3(-1, 0, 0)) * detectionRange, Color.red);
            }
        }       

    }        

    /// <summary>
    /// Chase the target once it has been caught in line of sight or through sound.
    /// Chase the player until attack distance is reached and then call attack method
    /// </summary>
    private void Chase()
    {
        if (Vector3.Distance(thisEnemy.transform.position, target.position) > attackDistance)
        {
            m_navMeshAgent.enabled = true;
            m_navMeshAgent.destination = target.position;                
        }
        else if (Vector3.Distance(thisEnemy.transform.position, target.position) < followDistance)
        {
            m_navMeshAgent.enabled = false;
            Attack();
        }
    }

    /// <summary>
    /// Call gun script Shoot() method. 
    /// Call gun script FireDelay method to see if can fire again.
    /// </summary>
    private void Attack()
    {
        if (target != null)
        {        
                        
            Debug.Log("Attack state");
                        
            if (canFire)
            {
                Shoot();
            }
            else
            {
                fireDelay();
            }
            
        }
    }
        
    /// <summary>
    /// Subtract damage from the enemy health if it is not already zero. 
    /// Call die method in this case.
    /// </summary>
    /// <param name="amount"></param>
    public void TakeDamage(float amount)
    {
        if (health > 0)
        {
            health -= amount;

            if (!targetDetected)
            {
                targetDetected = true;                
            }
        }
        else
        {
            Die();
        }
    }

    /// <summary>
    /// Destroy the gameobject when enemy dies.
    /// </summary>
    private void Die()
    {
        //Instantiate a blood spatter from array when enemy dies
        if(bloodSpatter != null)
        {
            Vector3 splaterPos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.9f, gameObject.transform.position.z);
            Instantiate(bloodSpatter[Random.Range(0, bloodSpatter.Length)], splaterPos, gameObject.transform.rotation);
        }

        //Destroy enemy game object
        Destroy(gameObject);
    }

    /// <summary>
    /// Shoot methods fires a raycast from the enemy weapon to the player.
    /// Can only run if the canFire bool is true. It is reset with the FireDelay() method
    /// </summary>    
    private void Shoot()
    {
        if (canFire)
        {
            RaycastHit shot;
            //RaycastHit shot;
            if (Physics.Raycast(thisEnemy.transform.position, thisEnemy.transform.forward, out shot, 40f))
            {
                //Reference to player
                RigidbodyFirstPersonController player = shot.transform.GetComponent<RigidbodyFirstPersonController>();

                //Apply damage to player
                player.TakeDamage(1f);

                //Set canFire to false to allow for FireDelay
                canFire = false;

                //Get instance of muzzleflash from weapon                
                ParticleSystem muzzleFlashLocal = weapon.GetComponent<Gun>().muzzleFlash;
                //Check muzzleFLash isnt null
                if (muzzleFlashLocal != null)
                {                                      
                    muzzleFlashLocal.Play();
                }
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public void BulletDeviation()
    {

    }
    /// <summary>
    /// Allow a fire delay after each weapon shot
    /// </summary>
    private void fireDelay()
    {
        
        timer += Time.deltaTime;

        if (timer > 2)
        {
            canFire = true;
            timer = 0f;
        }
    }   

    /// <summary>
    /// Receive messaage of player detection 
    /// set bool to initaiate the chase sequence
    /// </summary>
    /// <param name="detection"></param>
    public void EnemyDetected(bool detection)
    {
        targetDetected = detection;
    }
}




