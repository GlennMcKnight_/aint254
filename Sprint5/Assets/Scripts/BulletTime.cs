﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;


    public class BulletTime : MonoBehaviour
    {
        //Time variables for normal and slow time
        private float slowTime = 0.3f;
        private float normalTime = 1.0f;

        //Variables for length of ability and recharge
        public float abilityLength = 5.0f;
        public float rechargeTime = 5.0f;

        //Variables for timers and recharge bool
        private float abilityTimer = 0.0f;
        private float rechargeTimer = 0.0f;
        private bool isRecharge = true;
        private bool abilityUsed = false;

        //Variables for direction of bullet dodge and to check activation
        private int bltDodge = 1;
        private bool isDodge = false;

        //serialize field to expose in editor to add a reference to player 
        [SerializeField] private RigidbodyFirstPersonController player;
        private Rigidbody m_rigidbody;

       
        /// <summary>
        /// Check for input for slow motion key with Update function
        /// </summary>
        public void Update()
        {
            
            //If button is pressed call BulletTimrMode method to activate ability
            if (Input.GetKeyDown(KeyCode.J)) //GetKeyDown is most responsive for gameplay
            {
                BulletTimeMode();               
            }

            //If button is pressed call BulletDodge method to activate 2nd ability
            //Include direction for increased user control
            if (Input.GetKeyDown(KeyCode.K))
            {
                if (Input.GetKeyDown(KeyCode.K) && Input.GetKeyDown(KeyCode.W))
                {
                    bltDodge = 0;
                    BulletDodge();

                }else if (Input.GetKeyDown(KeyCode.K) && Input.GetKeyDown(KeyCode.D))
                {
                    bltDodge = 1;
                    BulletDodge();

                }else if (Input.GetKeyDown(KeyCode.K) && Input.GetKeyDown(KeyCode.A))
                {
                    bltDodge = 2;
                    BulletDodge();

                }else if (Input.GetKeyDown(KeyCode.K) && Input.GetKeyDown(KeyCode.S))
                {
                    bltDodge = 3;
                    BulletDodge();
                }
                else
                {
                    bltDodge = 0; //Set delault dodge to the right 
                    BulletDodge();
                }
            }


            //Call method to reset the ability if it has been used
            if (abilityUsed)
            {
                AbilityReset();                
            }

            //Call method to time ability recharge time
            if (!isRecharge)
            {
                RechargeAbility();
            }

        }
        
        /// <summary>
        /// Alter the games timescale for the special ability
        /// If the button is pressed again it will reset time to normal
        /// For eiher of these it sets the state to used and the ability must be recharged before use again
        /// </summary>
        private void BulletTimeMode()
        {

            if (abilityTimer <= abilityLength)
            {
                
                if (Time.timeScale == normalTime && isRecharge == true)
                {
                    Time.timeScale = slowTime;
                    Time.fixedDeltaTime = 0.02f * Time.timeScale;
                    isDodge = true; //Only relevant if the BulletDodge method has been called
                }

                else if (Time.timeScale == slowTime)
                {
                    Time.timeScale = normalTime;
                    Time.fixedDeltaTime = 0.02f * Time.timeScale;                    
                }
                else
                {
                    Debug.Log("Error with BulletTime");
                }

                abilityUsed = true;
                isRecharge = false;                
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        private void BulletDodge()
        {
            //Get rigidbody to apply force with new vector 3
            m_rigidbody = gameObject.GetComponent<Rigidbody>();
            m_rigidbody.drag = 0f;

            //Call BulletTimeMode method 
            BulletTimeMode();

            //If isDodge is true, which is set in BulletTimeMode method, make player dodge in specified direction
            if (isDodge)
            {
                switch (bltDodge)
                {
                    case 0:                        
                        m_rigidbody.AddForce(new Vector3(0.0f, 6.0f, 4.0f), ForceMode.Impulse);                        
                        break;

                    case 1:
                        m_rigidbody.AddForce(new Vector3(4.0f, 6.0f, 0.0f), ForceMode.Impulse); 
                        break;

                    case 2:
                        m_rigidbody.AddForce(new Vector3(-4.0f, 6.0f, 0.0f), ForceMode.Impulse); 
                        break;

                    case 3:
                        m_rigidbody.AddForce(new Vector3(0.0f, 6.0f, -4.0f), ForceMode.Impulse); 
                        break;

                    default:
                        Debug.Log("Bullet Dodge failed");
                        break;
                }

                isDodge = false;
            }
            
        }

        /// <summary>
        /// Reset the timescale to normal time if ability use runs to the end of its timer
        /// Reset ability timer
        /// </summary>
        private void AbilityReset()
        {
            if (abilityTimer <= abilityLength)
            {
                abilityTimer += Time.unscaledDeltaTime;                
            }
            else
            {
                Time.timeScale = normalTime;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
                abilityTimer = 0.0f;
                abilityUsed = false;
                isDodge = false;
            }
        }

        /// <summary>
        /// Time the recharge for the ability before itcan be used again
        /// Reset the recharge timer
        /// </summary>
        private void RechargeAbility()
        {
            if(rechargeTimer <= rechargeTime)
            {
                rechargeTimer += Time.unscaledDeltaTime;
            }
            else
            {
                isRecharge = true;
                rechargeTimer = 0.0f;
            }
        }
       
        public bool isBulletTime
        {
            get
            {
                return abilityUsed;
            }
        }
    }


