﻿using UnityEngine;
using System.Collections;

public class XBoxMapping : MonoBehaviour 
{
	void OnGUI()
	{
		GUI.Label(new Rect(5, 5, 200, 25), "Left Joystick X-Axis: ");
		GUI.Label(new Rect(155, 5, 200, 25), Input.GetAxis("Horizontal").ToString());
		
		GUI.Label(new Rect(5, 25, 200, 25), "Left Joystick Y-Axis: ");
		GUI.Label(new Rect(155, 25, 200, 25), Input.GetAxis("Vertical").ToString());
	}
}
