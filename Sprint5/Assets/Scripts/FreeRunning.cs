﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;


public class FreeRunning : MonoBehaviour
{    
    //Get transforms to fire the detection rays from
    public Transform hitBody;
    public Transform hitKnee;
    public Transform hitHead;
    public Transform hitRight;
    public Transform hitLeft;

    //Variables for wall running
    private bool isWallRunningR;
    private bool isWallRunningL;    
    private RaycastHit rayR;
    private RaycastHit rayL;
    private float maxHeight;
    bool isGrounded = true;
    float timer = 3f;

    //Bools to determine action
    private bool isHitBody = false;
    private bool isHitHead = false;
    private bool isHitKnee = false;
        
    //FreeRunning variables passed from FPS controller
    private bool isJumping;
    private bool isRunning;
    Rigidbody playerRB;

    //Determines if the player is sliding
    bool isSliding = false;


    // Use this for initialization
    void Start()
    {       
        playerRB = GetComponent<Rigidbody>();       
    }

    //Call update regularly
    private void FixedUpdate()
    {
        //Get up-to-date information on player status from RigidbodyFirstPersonController 
        isRunning = GetComponent<RigidbodyFirstPersonController>().Running;       
        isGrounded = GetComponent<RigidbodyFirstPersonController>().Grounded;

        //Check if player is running
        CheckRunning();
        
        //Set maxHeight a player can get too based on curent position
        if (isGrounded)
        {
            maxHeight = playerRB.transform.position.y + 3f;
        }
        
        //If wallrunning has strted then start the timer to determine when it should end
        if(isWallRunningL || isWallRunningR)
        {
            Timer();
        }
        else
        {
            timer = 4f;
        }
        

        //Stop the player jumping excessively high
        if(playerRB.transform.position.y >= maxHeight)
        {
            playerRB.transform.position = new Vector3(playerRB.transform.position.x, maxHeight, playerRB.transform.position.z);
        }

        /*
        //Reset the wallrun timer if the player jumps to another wall 
        //If the player jumps again, propel them twards the other wall
        if (Input.GetKeyDown(KeyCode.Space) && (isWallRunningL || isWallRunningR))
        {
            timer = 3f;

            if (isWallRunningL)
            {
                playerRB.AddForce(40, 0, 0);
            }
            if(isWallRunningR)
            {
                playerRB.AddForce(-40, 0, 0);
            }
        }
        */


        if (Input.GetKey(KeyCode.T))
        {
            Slide();
        }
    }
  
    /// <summary>
    /// 
    /// </summary>
    private void CheckRunning()
    {
        Debug.Log("Check running");
               
        if (isRunning)
        {
            Debug.Log("Free running - ISRUN");

            if (!isGrounded)
            {
                Debug.Log("Is wall running");
                WallRun();
            }
            else
            {
                FreeRunningRayCast();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void FreeRunningRayCast()
    {
        //Raycasts for fre running hit detection
        RaycastHit rayBody;
        RaycastHit rayHead;
        RaycastHit rayKnee;


        //Detect if an object is at head height
        if (Physics.Raycast(hitHead.transform.position, hitHead.transform.forward, out rayHead, 3f))
        {
            if (rayHead.transform.tag == "Wall")
            {
                isHitHead = true;
                Debug.Log("Ray cast HEAD is true");               
            }
        }
        else
        {
            isHitHead = false;
        }
        

        //Detect if an object is at waist height
        if (Physics.Raycast(hitBody.transform.position, hitBody.transform.forward, out rayBody, 3f))
        {
            if (rayBody.transform.tag == "Wall")
            {
                isHitBody = true;
                Debug.Log("Ray cast BODY is true");
            }            
        }
        else
        {
            isHitBody = false;
        }

        
        //Detect if an object is at knee height
        if (Physics.Raycast(hitKnee.transform.position, hitKnee.transform.forward, out rayKnee, 0.5f))
        {
            if (rayKnee.transform.tag == "Wall")
            {
                isHitKnee = true;
                Debug.Log("Ray cast KNEE is true");
            }
        }
        else
        {
            isHitKnee = false;
        }
        
        //Call method to determine if free running action is to be taken
        FreeRunningControl();             
    }

    /// <summary>
    /// Checks if the conditions hav been met to initiate a Slide or Vault 
    /// </summary>
    public void FreeRunningControl()
    {
        //If hit at knee and body then call Vault() method
        if (isHitKnee && (!isHitBody && !isHitHead && !isSliding))
        {
            Vault();
        }
        
        //If is hit body and head but not knee then call Slide() method
        if (!isHitKnee && (isHitBody && isHitHead))
        {
            Slide();
        }
    }

    /// <summary>
    /// Allows the player to vault over an obstacle automatically when running
    /// </summary>
    public void Vault()
    {
        Debug.Log("Is vaulting");




        //Everything needs to work in relation to the players forward position.!!!!!!!
        //Currently only works in relation to one plane of movement on the z axis!!!!!!






        //Variables for the manipultion of the player during the vault
        Vector3 vaultPosition = new Vector3(0f, 32f, 0f);
        Vector3 vaultReference = Vector3.zero;
        Quaternion vaultAngle = new Quaternion((playerRB.transform.rotation.x - 9f), playerRB.transform.rotation.y, playerRB.transform.rotation.z - 9f, playerRB.transform.rotation.w);
        float forward = 1f;

        //Check the player is moving forward to initiate vault
        if (Input.GetKey(KeyCode.W))
        {
            //Add rotation to simulate a vaulting motion
            playerRB.transform.rotation = Quaternion.Lerp(playerRB.transform.rotation, vaultAngle, Time.deltaTime * 0.75f);
            //Move the player up and over the obstacle
            playerRB.transform.position = Vector3.SmoothDamp(playerRB.transform.position, (playerRB.transform.position + vaultPosition), ref vaultReference, 0.16f);
            playerRB.transform.Translate(0f, 0f, 1f, Space.Self);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void Slide()
    {
        //Variable to determine when slide should finish
        bool slideFinished = false;

        Debug.Log("Is sliding");

        //Variables for manipulation of player during vault
        Vector3 slidePosition = new Vector3(playerRB.transform.position.x, playerRB.transform.position.y - 2f, playerRB.transform.position.z);
        Vector3 slideForward = new Vector3(playerRB.transform.position.x, playerRB.transform.position.y, 2f);
        Quaternion slideAngle = new Quaternion(playerRB.transform.rotation.x, -90f, playerRB.transform.rotation.z -90f, playerRB.transform.rotation.w);
        Vector3 slideReference = Vector3.zero;

        //Slide for the length of the Timer() method
       

            
        //Check the player is moving forward
        //if (Input.GetKey(KeyCode.W)){







            //Add rotation to simulate a slidingmotion
            //playerRB.transform.rotation = Quaternion.Lerp(playerRB.transform.rotation, slideAngle, Time.deltaTime * 0.75f);
            //playerRB.transform.Translate(0f, 0.2f, 0f, Space.Self);

            isSliding = true;
            CapsuleCollider m_capsule = GetComponent<CapsuleCollider>();
            Camera thisCam = GetComponent<Camera>();

            
            m_capsule.radius = 0.4f;
            m_capsule.height /= 5f;
            m_capsule.center = new Vector3(m_capsule.center.x, -0.55f, m_capsule.center.z);


            playerRB.AddForce(1f, 1f, 1f);

            //player.transform.height *= 0.5f; 






            //cam.transform.position = Vector3.Lerp(cam.transform.position, new Vector3(cam.transform.position.x, cam.transform.position.y /2, cam.transform.position.z), 5f);


            //playerRB.transform.Translate(0f, 0.1f, 0f, Space.Self);

            //m_capsule.enabled = false; 
            
            /*
            while (timer > -1000f)
            {
                

                timer -= Time.deltaTime;

               
            }

            m_capsule.radius = 0.5f;
            m_capsule.height *= 5f;
            m_capsule.center = new Vector3(0, 0, 0);
            */

            //m_capsule.enabled = true;

            //m_capsule.enabled = true;
            //cam.transform.position = playerRB.transform.position;



            //playerRB.transform.Translate(0f, 0.1f, 0f, Space.Self);
            //playerRB.transform.localScale *= 2.5f;





            /*
            //playerRB.transform.position = Vector3.SmoothDamp(playerRB.transform.position, (playerRB.transform.position + new), ref slideReference, 0.16f);

            playerRB.transform.Translate(0f, 0.2f, 0f, Space.Self);

            playerRB.transform.localScale *= 2.5f;
            isSliding = false;

            isHitBody = false;
            isHitHead = false;
            */
            //Move the player forward
            //playerRB.transform.position = Vector3.SmoothDamp(playerRB.transform.position, (playerRB.transform.position + slideForward), ref slideReference, 0.16f);

            //Timer();


            //playerRB.transform.localScale /= 2;
            //Change player position to slide under obstacle
            //playerRB.transform.position = Vector3.SmoothDamp(playerRB.transform.position, slidePosition, ref slideReference, 0.16f);


            //playerRB.transform.localScale *= 2; 





            //Check if the timer has finished
            // Timer(slideFinished);

       // }

        //Reset timer 
        timer = 1f;
        isSliding = false;
    }

    /// <summary>
    /// 
    /// </summary>
    private void WallRun()
    {
        if (Physics.Raycast(hitRight.transform.position, hitRight.transform.forward, out rayR, 3))
        {
            if (rayR.transform.tag == "Wall")
            {
                Debug.Log("Has hit object to right");

                playerRB.useGravity = false;
                isWallRunningR = false;
                isWallRunningL = true;
                Timer();
            }
        }
        else
        {
            playerRB.useGravity = true;
            isWallRunningR = false;
        }
        

        if (Physics.Raycast(hitLeft.transform.position, hitLeft.transform.forward, out rayL, 3))
        {
            if (rayL.transform.tag == "Wall")
            {
                Debug.Log("Has hit object to left");

                playerRB.useGravity = false;
                isWallRunningR = true;
                isWallRunningL = false;
                Timer();
            }
        }
        else
        {
            playerRB.useGravity = true;
            isWallRunningL = false;
        }
         
    }

    /// <summary>
    /// Reactivate gravity for player when timer runs out
    /// </summary>
    private void Timer()
    {
        timer -= Time.deltaTime;   
        
        if(timer <= 0)
        {
            playerRB.useGravity = true;
        }
    }
    
}

