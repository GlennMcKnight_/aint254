﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAlert : MonoBehaviour {

    private bool detected = false;

    private void Update()
    {
        if (detected)
        {
            BroadcastMessage("EnemyDetected", true);
        }
    }

    public void Detected(bool detection)
    {
        detected = detection;
    }

}
